import {
    Reviews
} from './collections';

import {
    Products
} from '../products/collections';

Meteor.methods({
    addReview: function (data) {

        let pushedReviews = Reviews.find({
            productId: data.productId
        });
        let numberOfPushedReviews = pushedReviews.count();

        let totalRating = 0;
        pushedReviews.forEach(element => {
            totalRating += element.rating
            // console.log(totalRating)
        });

        totalRating += data.rating;
        // console.log("1-" + totalRating)

        let resultRating = Math.round(totalRating / (numberOfPushedReviews + 1));
        // console.log("2-" + resultRating)

        let _id = data.productId

        Products.update({
            _id
        }, {
            $set: {
                avgRating: resultRating
            }
        })
        // console.log("3-" + testus)
        return Reviews.insert(data);
    },
    // removeReview(_id) {
    //     return Reviews.remove({
    //         _id: _id
    //     });
    // }
    // updateReview(_id,checked){
    //     return Reviews.update({_id},{$set:{isPrivate: checked}})
    // }
    getCount: function (productId) {
        return Reviews.find({
            productId: productId
        }).count()
    }
});