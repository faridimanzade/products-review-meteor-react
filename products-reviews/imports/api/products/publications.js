import {Products} from './collections';
import {Images} from "../images/collections";
import {Meteor} from "meteor/meteor";
import { publishComposite } from 'meteor/reywood:publish-composite';

// Meteor.publish('getProducts', function () {
//     return Products.find({});
// });

publishComposite('getProducts',function (){
    return {
        find() {
            return Products.find({});
        },
        children: [{
            find(product) {
                if(product.imageId){
                    return Images.find({_id: product.imageId}).cursor
                }
            },
        },]
    }
})
