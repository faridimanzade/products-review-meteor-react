import {
    Products
} from './collections';

// import {
//     Reviews
// } from '../reviews/collections';


Meteor.methods({
    addProduct: function (data) {
        return Products.insert(data);
    },
    removeProduct(_id) {
        return Products.remove({
            _id: _id
        });
    },
    // updateProduct(_id,checked){
    //     return Products.update({_id},{$set:{isPrivate: checked}})
    // }
    // getCountForEach: function (productId) {
    //     return Reviews.find({
    //         productId: productId
    //     }).count();
    // }
});