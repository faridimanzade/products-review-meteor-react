import React from 'react';
import { Navbar } from "../components/Navbar";
import { Aside } from "../components/Aside";
import { Footer } from "../components/Footer";
import { AddProduct } from "../pages/AddProduct";
import { Home } from "../pages/Home";
import { FeaturedProducts } from "../pages/FeaturedProducts";
import { AddReview } from "../pages/AddReview";
import { ProductReviews } from '../pages/Reviews'

import ReactDOM from "react-dom";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";





export const App = () => {
  return (
    <Router>

      <Navbar />

      <br />
      <div className="container">
        <div className="row justify-content-between">
          <div className="col-sm-3">
            <Aside />
          </div>
          <div className="col-sm-8">
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
              <Route exact path="/products">
                <FeaturedProducts />
              </Route>
              <Route exact path="/addProduct">
                <AddProduct />
              </Route>
              <Route exact path="/addReview/:_id">
                <AddReview />
              </Route>
              <Route exact path="/reviews/:_id">
                <ProductReviews />
              </Route>
            </Switch>
          </div>
        </div>
      </div>

      <br />

      <Footer />
    </Router>

  )
}
