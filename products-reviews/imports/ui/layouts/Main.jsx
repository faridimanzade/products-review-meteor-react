import React from 'react';

import {App} from './App';

export const Main = () => {
    return (
        <div>
            <App />
        </div>
    )
}
