import React from 'react';

export const Footer = () => {
    return (
        <footer className="bg-dark text-center text-lg-start">
            <div className="text-light text-center p-3" style={{backgroundColor: "rgba(0, 0, 0, 0.2)"}}>
                ©️ 2021 Copyright: &nbsp;  
                <a className="text-light" href="#">Farid Imanzade</a>
            </div>
        </footer>
    )
};