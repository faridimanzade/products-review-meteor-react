import React, { useState, Fragment } from 'react';

import { useTracker } from 'meteor/react-meteor-data';
import { Categories } from "../../api/categories/collections";
import {Link} from "react-router-dom";

export const Aside = () => {


    const categories = useTracker(() => {
        Meteor.subscribe('getCategories');
        const categories = Categories.find().fetch();
        return categories;
    });



    return (
        <div>
            <h2>Product Categories</h2>
            <div className="aside-container">
                {
                    categories&&categories.map(category =>(
                        <div key={category._id} className="aside-row">
                            <Link to={`/category/${category._id}`}>{category.name}</Link>
                            {/*<a href="/category/{category._id}" className="aside-row-nav">{category.name}</a>*/}
                        </div>
                    ))
                }
            </div>
        </div>
    )
};
