import React, {useState, Fragment} from 'react';
import {useHistory, useParams} from 'react-router-dom';
import {useTracker} from 'meteor/react-meteor-data';
import {NavLink} from "react-router-dom";

import {
    Reviews
} from '../../api/reviews/collections';
import {
    Products
} from '../../api/products/collections';
import {
    Images
} from '../../api/images/collections';


function StarFiller(rating = 0) {
    let arr = new Array(rating);

    for (let index = 0; index < rating; index++) {
        arr[index] = 1;
    }

    return arr;
}


export const ProductReviews = () => {

    let {_id} = useParams();
    const history = useHistory();
    const [count, setCount] = useState(0)

    const {product, ready, img, lastReviews, filledOnce, emptyOnce} = useTracker(() => {
        let filledOnce = [];
        let emptyOnce = [];

        let allSubscriptions = [Meteor.subscribe('getProducts'), Meteor.subscribe('getLastReviews', _id)];

        let allReady = allSubscriptions.every(handle => handle.ready())

        if (!allReady) return {ready: allReady}

        Meteor.call('getCount', _id, (err, res) => {
            if (err) alert(err)
            if (res) setCount(res)
        });

        const product = Products.findOne({
            _id: _id
        });
        const lastReviews = Reviews.find({
            productId: _id
        }, {
            sort: {
                dateTime: -1
            },
            limit: 3
        }).fetch();
        /*let handler = Meteor.subscribe('getImages');

        if (!handler.ready()) {
            loading = true
        } else {
            img = Images.findOne({
                _id: product.imageId
            })?.link();
        }*/


        if (product) {
            filledOnce = StarFiller(product?.avgRating)
            emptyOnce = StarFiller((5 - product?.avgRating))
        }


        return {
            product, ready: allReady, img: Images.findOne({
                _id: product.imageId
            })?.link(), lastReviews, filledOnce, emptyOnce
        };
    });

    return (
        <>
            {
                ready && product ? <>
                    <div className="card mb-3">


                        <div className="d-flex justify-content-center">
                            {
                                !ready ? <div>Loading...</div> :
                                    <img className="card-img-top col-sm-5" src={img} alt="Image Here"/>
                            }
                        </div>

                        <div className="card-body">
                            <h3 className="card-title">{product?.name}</h3>
                            <p className="card-text">{product?.description}</p>
                        </div>


                        <hr/>


                        <div className="d-flex justify-content-end px-3">
                            {/* <a className="btn btn-success" href="/addReview/{{_id}}">Leave a Review</a> */}
                            <NavLink exact className="btn btn-success" to={/addReview/ + product?._id}>
                                Leave a Review
                            </NavLink>
                        </div>


                        <div className="gradientStyle">

                            <h3 className="card-title px-3">Rating and Reviews</h3>

                            <div className="d-flex px-3 my-3 align-items-center">
                                <span>Average Rating: </span>
                                <div className="d-flex mx-2">
                                    {
                                        filledOnce.map((e, index) =>
                                            e ? <img key={index + 100} className="reviewStarts"
                                                     src="/images/golden-star.png" alt="Image Here"/> : null
                                        )
                                    }
                                    {
                                        emptyOnce.map((e, index) =>
                                            e ? <img key={index + 200} className="reviewStarts"
                                                     src="/images/white-star.png" alt="Image Here"/> : null
                                        )
                                    }
                                </div>
                                <span>({count})</span>
                            </div>

                            {
                                lastReviews.map(rev => {
                                    let filled = StarFiller(rev.rating)
                                    let empty = StarFiller((5 - rev.rating))

                                    return (<div key={rev._id} className="d-flex p-3 justify-content-between">
                                            <div className="d-flex align-items-center col-sm-8">
                                                {
                                                    filled.map((e, index) =>
                                                        <img key={index} className="reviewStarts"
                                                             src="/images/golden-star.png" alt="Image Here"/>
                                                    )
                                                }
                                                {
                                                    empty.map((z, index) =>
                                                        <img key={index} className="reviewStarts"
                                                             src="/images/white-star.png" alt="Image Here"/>
                                                    )
                                                }
                                                {/* <img className="reviewStarts" src="/images/golden-star.png" alt="Image Here" />
                                            <img className="reviewStarts" src="/images/white-star.png" alt="Image Here" /> */}
                                                <span className="text-break">{rev.text}</span>
                                            </div>
                                            <span
                                                className="col-sm-2">{moment(rev.dateTime).format("MM-DD-YYYY")}</span>
                                        </div>
                                    )
                                })
                            }


                        </div>
                    </div>

                </> : <>Loading</>
            }
        </>
    )
}
