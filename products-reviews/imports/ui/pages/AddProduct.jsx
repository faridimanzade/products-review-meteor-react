import React, { useState, Fragment } from 'react';
import { useHistory } from 'react-router-dom';
import { useTracker } from 'meteor/react-meteor-data';
import { NavLink } from "react-router-dom";



import {
    Images
} from '../../api/images/collections';
import {
    Categories
} from '../../api/categories/collections';



export const AddProduct = () => {

    const history = useHistory();


    const categories = useTracker(() => {

        Meteor.subscribe('getImages');
        Meteor.subscribe('getCategories');

        const categories = Categories.find().fetch();
        return categories;
    });


    function submitForm(event) {
        event.preventDefault();
        let target = event.target;
        let name = target.productName.value;
        let categoryId = target.productCategory.value;
        let description = target.productDescription.value;
        let isFeatured = target.productFeatured.value ? true : false;


        let file = document.getElementById('myFile').files[0]

        const upload = Images.insert({
            file,
            chunkSize: 'dynamic'
        }, false);

        upload.on('start', function () {

        });

        upload.on('end', function (error, fileObj) {
            if (error) {
                alert(`Error during upload: ${error}`);
            } else {
                console.log(`File "${fileObj.name}" successfully uploaded`);

                let product = {
                    name: name,
                    description: description,
                    isFeatured: isFeatured,
                    categoryId: categoryId,
                    imageId: fileObj._id,
                    avgRating: 0
                }

                let goToPage = history.push('/')
                if (product.isFeatured) goToPage = history.push("/products")

                insertProduct(product, goToPage);
            }
        });

        upload.start();
    }


    function insertProduct(product, goToPage) {
        Meteor.call('addProduct', product, function (error, success) {
            if (error) alert('error', error);
            if (success) goToPage
        });
    }


    return (
        <div className="addProductContainer">

            <h2>Add Product</h2>

            <form id="addProductForm" name="addProductForm" action="" onSubmit={submitForm}>
                <div className="form-group">
                    <p className="formInputHeader">Name</p>
                    <input className="form-control" name="productName" type="text" placeholder="Enter Name of the Product"
                        required="required" />
                </div>
                <div className="form-group">
                    <p className="formInputHeader">Category</p>
                    <select id="productCategory" className="form-select" name="productCategory" required="required">
                        <option value="0">-- Select Category --</option>
                        {
                            categories.map(category =>
                                <option key={category._id} value={category._id}>{category.name}</option>
                            )
                        }
                    </select>
                </div>
                <div className="form-group">
                    <p className="formInputHeader">Product Image</p>
                    <input id="myFile" className="form-control" type="file" required="required" />
                </div>
                <div className="form-group">
                    <p className="formInputHeader">Description</p>
                    <textarea id="productDescription" className="form-control" name="productDescription" rows="8"
                        placeholder="Enter brief description about product" required="required"></textarea>
                </div>

                <div className="form-group my-3">
                    <label style={{ marginRight: '15px' }}> Featured : </label>
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="radio" name="productFeatured" id="yes" value="true"
                            required="required" />
                        <label className="form-check-label" htmlFor="inlineRadio1">Yes</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="radio" name="productFeatured" id="no" value=""
                            required="required" />
                        <label className="form-check-label" htmlFor="inlineRadio2">No</label>
                    </div>
                </div>

                <div className="form-group my-3">
                    <button style={{ marginRight: '15px' }} className="btn btn-success" type="submit">Add Product</button>
                    {/* <a className="btn btn-warning" href="/">Cancel</a> */}
                    <NavLink exact className="btn btn-warning" to={"/"} >
                        Cancel
                    </NavLink>
                </div>
            </form>

        </div>
    )
} 