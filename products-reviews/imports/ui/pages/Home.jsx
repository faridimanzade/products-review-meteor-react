import React, { useState, Fragment } from 'react';
import { useTracker } from 'meteor/react-meteor-data';
import { NavLink } from "react-router-dom";



import {
    Images
} from '../../api/images/collections';
import {
    Products
} from '../../api/products/collections';
import {
    ProductsReviews
} from '../../api/productsReviews/collections';



// =========================== RETURNS ARRAY OF STARS OF RATING
function StarFiller(rating = 0) {
    let arr = new Array(rating);

    for (let index = 0; index < rating; index++) {
        arr[index] = 1;
    }

    return arr;
}


export const Home = () => {

    const { products } = useTracker(() => {

        Meteor.subscribe('getProducts');

        if (Products.find().count()) {
            ProductsReviews.remove()
            Products.find().map((elem) => {
                Meteor.call('getCount', elem._id, (err, res) => {
                    if (err) alert(err)
                    if (res != undefined) {
                        console.log(res)
                        ProductsReviews.insert({
                            productId: elem._id,
                            reviewCount: res
                        })
                    }
                });
            });
        }

        const products = Products.find({ isFeatured: false }).fetch();
        return { products };
    });

    const { loading } = useTracker(() => {
        let handler = Meteor.subscribe('getImages');

        if (!handler.ready()) {
            return { loading: true }
        }

        return { loading: false };
    });

    return (
        <div>
            {
                products.map(product => {

                    let image = Images.findOne({
                        _id: product.imageId
                    })?.link();

                    let filled = StarFiller(product.avgRating)
                    let empty = StarFiller((5 - product.avgRating))

                    return (<div key={product._id} className="card mb-3">
                        <div className="row g-0">
                            <div className="col-md-4 d-flex align-items-center p-2">
                                {
                                    loading ? <div>Loading...</div> : <img className="productImage" src={image} alt="Image Here" />
                                }
                            </div>
                            <div className="col-md-8">
                                <div className="card-body">
                                    <h5 className="card-title">{product.name}</h5>
                                    <div className="d-flex my-3 align-items-center">
                                        <span>Average Rating: </span>
                                        <div className="d-flex mx-2">
                                            {
                                                filled.map((e, index) =>
                                                    <img key={index} className="reviewStarts" src="/images/golden-star.png" alt="Image Here" />
                                                )
                                            }
                                            {
                                                empty.map((z, index) =>
                                                    <img key={index} className="reviewStarts" src="/images/white-star.png" alt="Image Here" />
                                                )
                                            }
                                        </div>
                                        <span>({product.avgRating})</span>
                                    </div>
                                    <p className="card-text">{product.description}</p>
                                    <div className="d-flex">
                                        {/* <a className="btn btn-secondary" href="/reviews/{product._id}">Reviews</a> */}
                                        <NavLink exact className="btn btn-secondary" to={"/reviews/" + product._id} >
                                             Reviews
                                        </NavLink>
                                        {/* <a className="btn btn-primary mx-2" href="/addReview/{product._id}">Add Review</a> */}
                                        <NavLink exact className="btn btn-primary mx-2" to={/addReview/ + product._id} >
                                             Add Review
                                        </NavLink>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>)
                })
            }
        </div>
    )
}