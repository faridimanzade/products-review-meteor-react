import React, { useState, Fragment } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useTracker } from 'meteor/react-meteor-data';
import { NavLink } from "react-router-dom";


import {
    Products
} from '../../api/products/collections'


export const AddReview = () => {
    
    const {_id} = useParams();
    const history = useHistory();
    
    
    const {product} = useTracker(() => {

        Meteor.subscribe('getProducts');
        Meteor.subscribe('getReviews');
        
        const product = Products.findOne({
            _id: _id
        });

        return {product};
    });


    function submitForm(event) {
        event.preventDefault();
        let target = event.target;
        let rating = target.reviewRating.value;
        let reviewContent = target.reviewContent.value;


        let review = {
            rating: parseInt(rating),
            text: reviewContent,
            productId : _id,
            dateTime: new Date()
        }

        let goToPage = history.push('/reviews/' + _id)

        Meteor.call('addReview', review, function(error, success) { 
            if (error) alert('error', error);  
            if (success) goToPage
        });
    }



    return(
        <div className="addReviewContainer">

        <h2>Add Review For &nbsp;
            <b>{product ? product.name : null}</b>
        </h2>

        <form id="addReviewForm" name="addReviewForm" action="" onSubmit={submitForm}>

            <div className="form-group">
                <p className="formInputHeader">Rating</p>
                <select id="reviewRating" className="form-select" name="reviewRating" required="required">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>

            <div className="form-group">
                <p className="formInputHeader">Review</p>
                <textarea id="reviewContent" className="form-control" name="reviewContent" rows="5"
                    placeholder="Enter brief description about Review" required="required"></textarea>
            </div>

            <div className="form-group my-3">
                <button style={{ marginRight: '15px' }} className="btn btn-success" type="submit">Add Review</button>
                {/* <a className="btn btn-warning" href="/">Cancel</a> */}
                <NavLink exact className="btn btn-warning" to={"/"} >
                    Cancel
                </NavLink>
            </div>
        </form>
    </div>
    )
}