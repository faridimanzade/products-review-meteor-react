import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import { Main } from '../imports/ui/layouts/Main'
import {
  BrowserRouter as Router,
} from "react-router-dom";

Meteor.startup(() => {
  render(<Main />, document.getElementById('react-target'));
});
